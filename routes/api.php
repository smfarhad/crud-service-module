<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'API\UserController@login');
Route::get('register', 'API\UserController@register');
Route::group(['middleware' => 'auth:api'], function(){
       Route::post('logout','API\UserController@logoutApi');
       Route::post('details', 'API\UserController@details');
       Route::apiResource('/v1/contacts', 'API\contact\v1\ContactsController');
       Route::post('/v1/contacts/batch-store', 'API\contact\v1\ContactsController@batchStore');
       Route::post('/v1/contacts/batch-update', 'API\contact\v1\ContactsController@batchUpdate');
       Route::post('/v1/contacts/batch-delete', 'API\contact\v1\ContactsController@batchDelete');
});


