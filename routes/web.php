<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::namespace('Admin')->prefix('admin')->name('admin.')->group(function () {
        Route::resource('/users', 'UsersController', ['except' => ['show', 'create', 'store']]);
        Route::resource('/contacts', 'ContactsController');
        
        Route::get('/batchcreate/{rows}', 'ContactsController@batchCreate')->name('batchcreate');
        Route::post('/batchcreate', 'ContactsController@batchStore')->name('batchcreate');
        
        Route::get('/batchupdate/{id}', 'ContactsController@batchEdit')->name('batchupdate');
        Route::post('/batchupdate', 'ContactsController@batchUpdate')->name('batchupdate');        
        
        Route::delete('/batchdelete', 'ContactsController@batchDelete')->name('batchdelete');
        
});


