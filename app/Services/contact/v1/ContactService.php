<?php

namespace App\Services\contact\v1;

use Illuminate\Support\Facades\Validator;
use App\Contact;
use Mavinoo\LaravelBatch\LaravelBatchFacade as Batch;
class ContactService {

    protected $singleValidationRules = [
        'name' => 'required',
        'email' => 'required|email|unique:contacts',
        'phone' => 'required|numeric',
        'city' => 'required',
        'country' => 'required'
    ];
    protected $batchValidationRules = [
        'contact.*.name' => 'required',
        'contact.*.email' => 'required|email|distinct|unique:contacts',
        'contact.*.phone' => 'required|numeric',
        'contact.*.city' => 'required',
        'contact.*.country' => 'required',
    ];

    /**
     *  
     * validate all single input data request  
     * Two parameners fist request 
     * 2nd check create or update must be boolean
     *  
     */
    public function validate($contact, $contactId, $update = false) {
        $rules = [];
        if ($update) {
            $rules = $this->singleValidationRules;
            $rules['email'] = 'required|string|unique:contacts,email,' . $contactId;
        } else {
            $rules = $this->singleValidationRules;
        }

        $validator = Validator::make($contact, $rules);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        return true;
    }

    /**
     *  
     * validate all batch input data request  
     * Two parameners fist request 
     * 2nd check create or update
     * 2nd param Type boolean
     *  
     */
    public function batchValidate($contacts, $update = false) {
        $rules = [];
        if ($update) {
            $rules = $this->batchValidationRules;
            $rules['contact.*.email'] = 'required|string|email|distinct' ;
        } else {
            $rules = $this->batchValidationRules;
        }

        return Validator::make($contacts, $rules);
    }

    /**
     * Store batch newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     * */
    public function batchCreateContact($request) {
        if (Contact::insert($request)) {
            return true;
        }
        return false;
    }
    
 /**
     * Update batch resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     * */
    public function batchUpdateContact($request) {
        $contact = new Contact;
        $input = $request;
        if (Batch::update($contact, $input, 'id')) {
            return true;
        }
        return false;
    }
    
     /**
     * Remove a batch of resource from storage.
     *
     * @param  int  $id
     */
  public function batchDeleteContact($contactIds) {
        if (Contact::whereIn('id', explode(",", $contactIds))->delete()) {
           return true;
        } 
        return false;
    }
    
    /**
     * 
     * Display All resource
     * 
     * @return response
     */
    public function getContacts() {
        $data = $this->filterContact(Contact::get());
        return $data;
    }

    /**
     * 
     * Display the specified resource.
     *
     * @param  int  $id
     * @return response
     */
    public function getContact($id) {
        $data = $this->filterContact(Contact::where(['id' => $id])->get());
        return $data;
    }

    /*
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     */

    public function createContact($request) {

        $contact = new Contact;

        $contact->name = $request['name'];
        $contact->email = $request['email'];
        $contact->phone = $request['phone'];
        $contact->city = $request['city'];
        $contact->country = $request['country'];

        if ($contact->save()) {
            return response()->json(['success' => $contact->name . ' Has been Inserted Successfully.'], 201);
        }
        return response()->json(['error' => ' Data insertion error.'], 500);
    }

    /*
     * 
     * Update contact contact 
     * 
     */

    public function updateContact($request, $id) {

        $contact = Contact::find($id);

        $contact->name = $request['name'];
        $contact->email = $request['email'];
        $contact->phone = $request['phone'];
        $contact->city = $request['city'];
        $contact->country = $request['country'];

        if ($contact->save()) {
            return response()->json(['success' => $contact->name . ' Has been updated successfully.'], 201);
        }
        return response()->json(['error' => ' Data update error.'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function deleteContact($id) {
        $contact = Contact::find($id);
        if ($contact != null) {
            return $contact->delete();
        }
        return false;
    }

    /**
     * 
     * Display processed data specified resource.
     *
     * @return response
     */
    public function filterContact($contacts) {
        $data = [];
        foreach ($contacts as $contact) {
            $filtered = [
                'id' => $contact->id,
                'name' => $contact->name,
                'email' => $contact->email,
                'phone' => $contact->phone,
                'city' => $contact->city,
                'country' => $contact->country,
                'href' => route('contacts.show', $contact->id),
            ];
            $data[] = $filtered;
        }
        return $data;
    }

}
