<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Contact;
use App\Role;
use Gate;
use App\Services\contact\v1\ContactService;

class ContactsController extends Controller {

    /**
     * Create protected property 
     * to get contact data
     */
    protected $contacts;

    /**
     * load and init service data
     * for contact service data 
     */
    public function __construct(ContactService $contat) {
        $this->contacts = $contat;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if (Gate::any(['manage-contacts','manage-batch-contacts'])) {
            $contacts = Contact::paginate(5);
            return view('admin.contact.index')->with('contacts', $contacts)
                            ->with('numOfRow', 1);
        }else {
            abort(403, 'Unauthorized action.');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        if (Gate::any(['manage-contacts','manage-batch-contacts'])) {
            return view('admin.contact.create');
        }else {
            abort(403, 'Unauthorized action.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        if (Gate::any(['manage-contacts','manage-batch-contacts'])) {
            $validator = $this->contacts->validate($request->all());
            if ($validator === true) {
                $contact = $this->contacts->createContact($request->all());
                session()->flash('success', $request->name . ' Has been Inserted Successfully.');
            }

            session()->flash($validator);
            return $validator;
        }else {
            abort(403, 'Unauthorized action.');
        }
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contact) {
        if (Gate::any(['manage-contacts','manage-batch-contacts'])) {
            $roles = Role::all();
            $user = User::find($contact->id);
            $data = [
                'user' => $user,
                'roles' => $roles,
                'contact' => $contact
            ];
            return view('admin.contact.view')->with($data);
        }else {
            abort(403, 'Unauthorized action.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact) {
        if (Gate::any(['manage-contacts','manage-batch-contacts'])) {
            $roles = Role::all();
            $data = [
                'roles' => $roles,
                'contact' => $contact
            ];
            return view('admin.contact.edit')->with($data);
        }else {
            abort(403, 'Unauthorized action.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact) {
        if (Gate::any(['manage-contacts','manage-batch-contacts'])) {
            $validator = $this->contacts->validate($request->all(),$contact->id, true);
            if($validator === true) {
                $contacts = $this->contacts->updateContact($request->all(), $contact->id);
                session()->flash('success', $contact->name . ' Has Been Updated.');
                return response()->json($contacts, 201);
            }
            session()->flash('error', ' Testing error flash message');
            return $validator;
        }else {
            abort(403, 'Unauthorized action.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact) {
        if (Gate::any(['manage-contacts','manage-batch-contacts'])) {
            $this->contacts->deleteContact($contact->id);
            if (!$this->contacts->deleteContact($contact->id)) {
                session()->flash('success', $contact->name . ' Has Been Deleted Successfully');
                return response()->json(['success' => 'Data deleted successfuly'], 204);
            }
            return response()->json(['error' => 'Found Problem at deleting'], 500);
        }else {
            abort(403, 'Unauthorized action.');
        }
    }

    /**
     * Show the form for creating a  batch of new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function batchCreate($numOfRow) {
        if (Gate::allows('manage-batch-contacts')) {
            return view('admin.contact.batchCreate')
                            ->with('numOfRows', $numOfRow);
        }else {
            abort(403, 'Unauthorized action.');
        }               
    }
    
      /**
     * Store multiple record newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function batchStore(Request $request) {
        if (Gate::allows('manage-batch-contacts')) {
            //return $request->all()['contact'];
            $validator = $this->contacts->batchValidate($request->all());
            if ($validator->fails()) {
                return response()->json([ 'errors' =>  $validator->errors() ], 401);
            }else {
            // Contact::insert($request->all()['contact']);
                $this->contacts->batchCreateContact($request->all()['contact']);
                session()->flash('success', 'Data Has Been Inserted Successfully');
                return response()->json(['success' => 'Data Has Been Inserted Successfully'], 201);
            }
            session()->flash('error', 'Testing error flash message');
            return response()->json([ 'errors' =>  'Server Not Found' ], 500);
        }else {
            abort(403, 'Unauthorized action.');
        }   
    }
    
   /**
     * Show the form for editing the resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    
    public function batchEdit($id) {
        if (Gate::allows('manage-batch-contacts')) {
            $contacts = Contact::whereIn('id', explode(",", $id))->get();
            return view('admin.contact.batchEdit')
                            ->with('contacts', $contacts);
        } else {
            abort(403, 'Unauthorized action.');
        } 
    }
    /**
    * batch update resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\Contact  $contact
    * @return \Illuminate\Http\Response
    **/
    public function batchUpdate(Request $request) {
        if (Gate::allows('manage-batch-contacts')) {
            $validator = $this->contacts->batchValidate($request->all(), true);
            
            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 401);
            }
            if ($this->contacts->batchUpdateContact($request->all()['contact'])){
                session()->flash('success', 'Data Has Been Updated Successfully');
                return response()->json(['success' => 'Data Has Been Inserted Successfully'], 201);
            } else {
                session()->flash('error', 'Nothing is changed to update');
                return response()->json(['errors' => 'Nothing is changed to update'], 500);
            }
            return response()->json([ 'errors' =>  'Server Not Found' ], 500);
        }else {
            abort(403, 'Unauthorized action.');
        } 
    }
    
    /**
     * Remove the batch resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    
    
    public function batchDelete(Request $request) {
        if (Gate::allows('manage-batch-contacts')) {
            $contactIds = $request->all()['contactIds'];
            if ($this->contacts->batchDeleteContact($contactIds)) {
                session()->flash('success', ' Data Is Deleted Successfully');
                return response()->json(['success' => 'Data Is Deleted Successfully'], 200);
            } else {
                session()->flash('error', 'Deleted Is Not Deleted Successfully');
                return response()->json(['error' => 'Deleted Is Not Deleted Successfully'], 200);
            }
        } else {
            abort(403, 'Unauthorized action.');
        } 
    }

}
