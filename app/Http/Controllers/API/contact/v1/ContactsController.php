<?php

namespace App\Http\Controllers\API\contact\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\contact\v1\ContactService;
use App\Contact;
use Gate;
class ContactsController extends Controller {

    /**
     * Create protected property 
     * to get contact data
     */
    protected $contacts;

    /**
     * load and init service data
     * for contact service data 
     */
    public function __construct(ContactService $contat) {
        $this->contacts = $contat;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if (Gate::any(['manage-contacts','manage-batch-contacts'])) {
            $data = $this->contacts->getContacts();
            return response()->json($data, 200);
        }else {
            abort(403, 'Unauthorized action.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        if (Gate::any(['manage-contacts','manage-batch-contacts'])) {
            $validator = $this->contacts->validate($request->all());

            if ($validator === true) {
                $contacts = $this->contacts->createContact($request->all());
                return response()->json($contacts, 201);
            }
            return $validator;
        }else {
            abort(403, 'Unauthorized action.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        if (Gate::any(['manage-contacts','manage-batch-contacts'])) {
            $data = $this->contacts->getContact($id);
            return response()->json($data);
        }else {
            abort(403, 'Unauthorized action.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        if (Gate::any(['manage-contacts','manage-batch-contacts'])) {
            $validator = $this->contacts->validate($request->all(), $id, true);
            if ($validator === true) {
                $contacts = $this->contacts->updateContact($request->all(), $id);
                return response()->json($contacts, 201);
            }
            return $validator;
        }else {
            abort(403, 'Unauthorized action.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (Gate::any(['manage-contacts','manage-batch-contacts'])) {
            $this->contacts->deleteContact($id);
            if (!$this->contacts->deleteContact($id)) {
                return response()->json(['success' => 'Data deleted successfuly'], 204);
            }
            return response()->json(['message' => 'Found Problem in deleting'], 500);
        }else {
            abort(403, 'Unauthorized action.');
        }
    }

    /**
     * Store multiple record newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function batchStore(Request $request) {
        if (Gate::any(['manage-batch-contacts'])) {
            $validator = $this->contacts->batchValidate($request->all());
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()], 401);
            } else {
                $this->contacts->batchCreateContact($request->all());
                return response()->json(['success' => 'Data Has Been Inserted Successfully'], 201);
            }
            return response()->json(['errors' => 'Server Not Found'], 500);
        }else {
            abort(403, 'Unauthorized action.');
        }
    }


    /**
     * batch update resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     * */
    public function batchUpdate(Request $request) {
        if (Gate::any(['manage-batch-contacts'])) {
            $validator = $this->contacts->batchValidate($request->all(), true);
            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 401);
            }
            if ($this->contacts->batchUpdateContact($request->all())) {

                return response()->json(['success' => 'Data Has Been Updated Successfully'], 204);
            } else {
                return response()->json(['errors' => 'Nothing is changed to update'], 500);
            }
            return response()->json(['errors' => 'Server Not Found'], 500);
        }else {
            abort(403, 'Unauthorized action.');
        }
    }

    public function batchDelete(Request $request) {
        if (Gate::any(['manage-batch-contacts'])) {
            $contactIds = $request->all()['Ids'];
            if ($this->contacts->batchDeleteContact($contactIds)) {
                return response()->json(['success' => 'Data Is Deleted Successfully'], 204);
            } else {
                return response()->json(['error' => 'Deleted Is Not Deleted Successfully'], 401);
            }
        }else {
            abort(403, 'Unauthorized action.');
        }
    }

}
