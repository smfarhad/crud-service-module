<?php

namespace App\Providers\contact\v1;

use Illuminate\Support\ServiceProvider;
use App\Services\flight\v1;

class ContactServiceProvider extends ServiceProvider {

    /**
     * Register services.
     *
     * @return void
     */
    public function register() {
        $this->app->bind(ContactService::class, function() {
            return new FlghtService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot() {
        //
    }

}
