<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
         'App\Contact' => 'App\Policies\ContactPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        
        $this->registerPolicies();

        //define Gates user gates
        Gate::define('manage-users', function($user){
            return $user->hasRole('admin');
        });
        Gate::define('edit-users', function($user){
            return $user->hasAnyRoles(['admin']);
        });
        Gate::define('delete-users', function($user){
            return $user->hasAnyRoles(['admin']);
        });
        
        // Define Contact Gate 
          //define Gates user gates
        Gate::define('manage-contacts', function($user){
            return $user->hasAnyRoles(['admin','user']);
        });
        Gate::define('view-contacts', function($user){
            return $user->hasAnyRoles(['admin','user']);
        });
        Gate::define('create-contacts', function($user){
            return $user->hasAnyRoles(['admin','user']);
        });
        Gate::define('edit-contacts', function($user){
            return $user->hasAnyRoles(['admin','user']);
        });
        Gate::define('delete-contacts', function($user){
            return $user->hasAnyRoles(['admin','user']);
        });
        Gate::define('manage-batch-contacts', function($user){
            return $user->hasRole('admin');
        });
        // Gate::define('batch-create-contacts', function($user){
        //     return $user->hasRole('admin');
        // });
        // Gate::define('batch-edit-contacts', function($user){
        //     return $user->hasRole('admin');
        // });
        // Gate::define('batch-delete-contacts', function($user){
        //     return $user->hasRole('admin');
        // });
        // passport routes
        Passport::routes();

    }
}
