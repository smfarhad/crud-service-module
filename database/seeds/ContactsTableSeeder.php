<?php

use Illuminate\Database\Seeder;
use App\Contact;

class ContactsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $contacts = [[
        'name' => 'SM Farhad Hossain',
        'email' => 'farhad1556@gmail.com',
        'phone' => '01729963312',
        'city' => 'Dhaka',
        'country' => 'Bangladesh',
            ], [
                'name' => 'Tanvir Sultan',
                'email' => 'tanvir@gmail.com',
                'phone' => '0171665321',
                'city' => 'Dhaka',
                'country' => 'Bangladesh',
            ], [
                'name' => 'Rony Hossain',
                'email' => 'rony@gmail.com',
                'phone' => '01738654451',
                'city' => 'Dhaka',
                'country' => 'Bangladesh',
            ], [
                'name' => 'Tareq Hasan',
                'email' => 'tareq@gmail.com',
                'phone' => '016845895387',
                'city' => 'Dhaka',
                'country' => 'Bangladesh',
            ], [
                'name' => 'Shamim Rahman',
                'email' => 'shamim@gmail.com',
                'phone' => '016845895387',
                'city' => 'Dhaka',
                'country' => 'Bangladesh',
            ], [
                'name' => 'Badhon Shaha',
                'email' => 'badhon@gmail.com',
                'phone' => '01572298845',
                'city' => 'Dhaka',
                'country' => 'Bangladesh',
            ], [
                'name' => 'Faiaz Hasan',
                'email' => 'faiaz@faiaz.com',
                'phone' => '01572298845',
                'city' => 'Dhaka',
                'country' => 'Bangladesh',
            ], [
                'name' => 'Abdul Kader',
                'email' => 'abdulkader@gmail.com',
                'phone' => '01572298845',
                'city' => 'Dhaka',
                'country' => 'Bangladesh',
            ], [
                'name' => 'Abdus Salam',
                'email' => 'abdussalam@gmail.com',
                'phone' => '01572298845',
                'city' => 'Dhaka',
                'country' => 'Bangladesh',
            ], [
                'name' => 'Newas Sharif',
                'email' => 'newas@gmail.com',
                'phone' => '01572298845',
                'city' => 'Dhaka',
                'country' => 'Bangladesh',
            ], [
                'name' => 'Mohon Kahn',
                'email' => 'mohon@gmail.com',
                'phone' => '01572298845',
                'city' => 'Dhaka',
                'country' => 'Bangladesh',
            ], [
                'name' => 'Shovon Mia',
                'email' => 'shovo@gmail.com',
                'phone' => '01572298845',
                'city' => 'Dhaka',
                'country' => 'Bangladesh',
            ], [
                'name' => 'Sagor Khan',
                'email' => 'Sagor@gmail.com',
                'phone' => '01572298845',
                'city' => 'Dhaka',
                'country' => 'Bangladesh',
            ],[
                'name' => 'Mir Hossain',
                'email' => 'mirhossain@gmail.com',
                'phone' => '01572298845',
                'city' => 'Dhaka',
                'country' => 'Bangladesh',
            ]
        ];
        Contact::insert($contacts);
    }

}
