/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 **/
require("./bootstrap");
require("jquery");
// ES6 Modules or TypeScript
import Swal from 'sweetalert2';

window.Vue = require("vue");

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component(
    "example-component",
    require("./components/ExampleComponent.vue").default
);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: "#app"
});

$(document).ready(function () {
    var numOfRow, checkAll, input;
    numOfRow = $("div.batch-input-container").length;
    input = $("input");
    $('a.batch-create').on('click', function (e) {
        e.preventDefault();
        const {
            value: number
        } = Swal.fire({
            title: 'Number of contact you want create',
            input: 'number',
            inputPlaceholder: 'Insert a number ',
            inputValidator: (value) => {
                if (!value) {
                    return 'Please insert a number';
                } else {
                    window.location($(this).prop('href') + '/' + value);
                }
            }
        });
    });

    // check all button selector 

    checkAll = $("input#checkAllCheckBox");


    // On click ad neew button 

    $("button.addNewRow").click(function () {
        addRow();
    });



    // Add new row method

    function addRow() {
        var newRow =
            '<div class="row batch-input-container">' +
            '<div class=" col-md-3 ">' +
            '<input type="text" class="form-control form-control-sm" name="contact[' + numOfRow + '][name]" required>' +
            "</div>" +
            '<div class="col-md-2">' +
            '<input type="email" class="form-control form-control-sm" name="contact[' + numOfRow + '][email]"  required>' +
            "</div>" +
            '<div class = "col-md-2" >' +
            '<input id = "phone"type = "text"class = "form-control form-control-sm " name = "contact[' + numOfRow + '][phone]" required>' +
            "</div>" +
            '<div class="col-md-2">' +
            '<input type = "text" class = "form-control form-control-sm"  name = "contact[' + numOfRow + '][city]"  required>' +
            "</div>" +
            '<div class = "col-md-2">' +
            '<input type = "text" class = "form-control-sm form-control" name = "contact[' + numOfRow + '][country]">' +
            "</div> " +
            '<div class = "col-md-1">' +
            '<button type="button" class ="removeRow btn btn-sm btn-danger"> X </button>' +
            "</div>" +
            "</div>";
        $("div#dynamic-container").append(newRow);
        numOfRow++;
    }


    // Auto hide success message field
    $("div.alert-success").delay(2000).slideUp(500);

    // On click remove row 

    $("form#batch-form").on("click", ".removeRow", function () {

        if (numOfRow > 1) {
            $(this).closest("div.row").remove();
            numOfRow--;
        } else {
            alert("You can not delete this row");
        }
    });

    // Batch update operation 

    $("a#batchUpdate").on("click", function (e) {
        e.preventDefault();
        var url, contactIds;
        url = '/admin/batchupdate/';
        contactIds = $("input.batch-checkbox:checkbox:checked").map(function () {
            return this.value;
        }).toArray();
        if (contactIds.length > 0) {
            window.location(url + "" + contactIds);
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'First you need to select something.',
                footer: 'Check at least one or more filed'
            });
        }
    });

    // Delete operation 

    $("table.table").on('click', '.delete-button', function (e) {
        e.preventDefault();
        var url, csrf_token, contactId;
        url = $(this).prop('action');
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                csrf_token = $('meta[name="csrf-token"]').prop('content');
                contactId = '';
                $.ajax({
                    url: url,
                    method: "DELETE",
                    data: "_token=" + csrf_token
                }).done(function (msg) {
                    window.location('/admin/contacts');
                }).fail(function (jqXHR, textStatus) {
                    console.log("Request failed: " + textStatus);
                });

            }
        });
    });

    // Batch Delete operation 

    $("a#batchDelete").on("click", function (e) {
        var url, csrf_token, contactIds;
        e.preventDefault();
        url = "/admin/batchdelete";
        csrf_token = $('meta[name="csrf-token"]').prop('content');
        contactIds = $("input.batch-checkbox:checkbox:checked").map(function () {
            return this.value;
        }).toArray();
        if (contactIds.length > 0) {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: url,
                        method: "DELETE",
                        data: "_token=" + csrf_token + "&contactIds=" + contactIds
                        // dataType: "html"
                    }).done(function (msg) {
                        window.location('/admin/contacts');
                    }).fail(function (response, textStatus) {
                        console.log("Request failed: " + textStatus);
                    });

                }
            });
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'First you need to select something.',
                footer: 'Check one or more filed'
            });
        }

    });


    /*
     * On check all checked field and 
     * on unchecked un checked check field
     */

    $(checkAll).on("click", function () {
        var $this = $(this);
        if ($this.val() == 1) {
            $("input.batch-checkbox").prop('checked', true);
            $this.val(0);
        } else {
            $("input.batch-checkbox").prop('checked', false);
            $this.val(1);
        }

    });

    /**
     * 
     * Open Single add new Modal
     * 
     * 
     **/
    $('#singleCreate').on("click", function (e) {
        e.preventDefault();
        var singleModel = $('#singleModal');
        $('#update-field').empty();
        singleModel.modal('show');
        removeValidationClass();
        $('input#id').val('');
        $('input#name').val('');
        $('input#email').val('');
        $('input#phone').val('');
        $('input#city').val('');
        $('input#country').val('');

        $('#singleModalTitle').text('Add New Cotact');
        $('#singleModal').find('form').prop('action', $(this).data('action'));
        $('#update-field').empty();

    });

    /**
     * @click Open edit modal 
     * set all necessary attr to form field and
     *  execute ajax request
     * 
     * 
     **/

    $('a.edit-button').on('click', function (e) {
        e.preventDefault();
        var singleModal, contact, action, putRequire;
        singleModal = $('#singleModal');

        $('#singleModalTitle').text('Edit Cotact');
        contact = $(this).data("contact");
        action = $(this).data("action");
        removeValidationClass();

        singleModal.modal('show');
        singleModal.find('form').prop('action', action);
        var putRequire = '<input name="_method" type="hidden" value="PUT">' +
            '<input name="id" type="hidden" value=' + contact.id + '>';
        $('#update-field').empty().append(putRequire);

        $('input#id').val(contact.id);
        $('input#name').val(contact.name);
        $('input#email').val(contact.email);
        $('input#phone').val(contact.phone);
        $('input#city').val(contact.city);
        $('input#country').val(contact.country);

    });

    /**
     * @click Open edit modal 
     *  and edit user 
     * 
     **/

    $('a.edit-user-button').on('click', function (e) {
        e.preventDefault();
        var singleModal, user, userRole, role, action, putRequire;
        singleModal = $('#singleModal');

        $('#singleModalTitle').text('Edit User');
        user = $(this).data("user");
        role = $(this).data("role");
        action = $(this).data("action");

        removeValidationClass();
        checkUserRole(role);

        singleModal.modal('show');
        singleModal.find('form').prop('action', action);
        var putRequire = '<input name="_method" type="hidden" value="PUT">' +
            '<input name="id" type="hidden" value=' + user.id + '>';
        $('#update-field').empty().append(putRequire);

        $('input#id').val(user.id);
        $('input#name').val(user.name);
        $('input#email').val(user.email);
    });


    // Save ad and edit 
    //  Firewith modal save button 
    $('#saveContact').on("click", function (e) {
        e.preventDefault();

        var contactForm, contactFormUrl, contactFormData;
        contactForm = $(this).closest("form"); // .serialize();
        contactFormUrl = contactForm.prop('action');
        contactFormData = contactForm.serialize();
        $.ajax({
            url: contactFormUrl,
            method: "POST",
            data: contactFormData
        }).done(function (response) {
            $('#singleModal').modal('hide');
            window.location('/admin/contacts');
        }).fail(function (jqXHR, textStatus) {
            checkValidation(jqXHR, 'name');
            checkValidation(jqXHR, 'email');
            checkValidation(jqXHR, 'phone');
            checkValidation(jqXHR, 'city');
            checkValidation(jqXHR, 'country');
        });
    });



    // Bach Save add and edit Fire 

    $('button#batch-save').on("click", function (e) {
        e.preventDefault();

        var contactForm, contactFormUrl, contactFormData;
        contactForm = $(this).closest("form"); // .serialize();
        contactFormUrl = contactForm.prop('action');
        contactFormData = contactForm.serialize();
        $.ajax({
            url: contactFormUrl,
            method: "POST",
            data: contactFormData
        }).done(function (response) {

            window.location('/admin/contacts');
            $("div.alert-danger").hide();

        }).fail(function (jqXHR, textStatus) {
            $("div.alert-danger").show();

            var errors = jqXHR.responseJSON.errors;
            if ($.isPlainObject(errors)) {
                $("ul#batch-name").empty();
                $.each(errors, function (key, value) {
                    //  console.log(value[0]);  
                    $("ul#batch-name").append("<li>" + value[0].replace(/contact./, ' ') + " </li>");

                });
            } else {
                $("div.alert-danger .col-11").text(jqXHR.responseJSON.errors);
            }



        });
    });

    // Checking User Input Validation 
    function checkValidation(jqXHR, inputFieldId) {
        var inputField, inputFieldContainer, inputFieldAlertbox;
        inputField = $("input#" + inputFieldId);
        inputFieldContainer = inputField.closest(".form-group");
        inputFieldAlertbox = inputFieldContainer.find(".invalid-feedback");
        if (jqXHR.responseJSON.error.hasOwnProperty(inputFieldId)) {
            inputField.addClass("is-invalid");
            inputFieldAlertbox.show();
            inputFieldContainer.find(".invalid-feedback strong").text(jqXHR.responseJSON.error[inputFieldId]);
        } else {
            inputField.removeClass("is-invalid");
            inputFieldAlertbox.hide();
        }
    }


    // Remove validation class
    function removeValidationClass() {
        input.removeClass("is-invalid");
        input.closest(".form-group").find(".invalid-feedback strong").text('');
    }

    // Check user role exist or not 
    function checkUserRole(role) {
        $('input.form-check-input').prop("checked", false);
        for (var i = 0; i < role.length; i++) {
            switch (role[i].pivot.role_id) {
                case 1:
                    $('#checkBox1').prop("checked", true);
                    break;
                case 2:
                    $('#checkBox2').prop("checked", true);
                    break;
                case 3:
                    $('#checkBox3').prop("checked", true);
                    break;
                default:
                    alert('No User Role Found!');
            }
        }
    }


});
