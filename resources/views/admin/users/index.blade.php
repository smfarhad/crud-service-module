@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif

            @if (session('error'))
            <div class="alert alert-danger" role="alert">
                {{ session('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            <div class="card">
                <div class="card-header">Contact List</div>
                <div class="card-body">

                    <table class="table">
                        <thead>
                            <tr>
                                <th scopr="col">#</th>
                                <th scopr="col">Name</th>
                                <th scopr="col">Email</th>
                                <th scopr="col">Roles</th>

                                <th scopr="col">
                                    @canany(['edit-users','delete-users'])
                                    Actions
                                    @endcan
                                </th>

                            </tr>
                        </thead>
                        <tbody> 
                            @foreach($users as $user)
                            <tr>
                                <td socket="row"> {{$user->id}} </td>
                                <td> {{$user->name}} </td>
                                <td> {{$user->email}} </td>
                                <td>
                                    {{implode(',',$user->roles()->get()->pluck('name')->toArray())}} 
                                </td>
                                <td> 
                                    @can('edit-users')
                                    <a data-user="{{$user}}" data-role="{{$user->roles}}" data-action="{{route('admin.users.update', $user->id)}}"   href="{{route('admin.users.edit', $user->id)}}" class="btn btn-sm btn-warning edit-user-button float-left">
                                        Edit
                                    </a>
                                    @endcan
                                    @can('delete-users')
                                    <form action="{{route('admin.users.destroy', $user->id)}}" method="post"  class="float-left">
                                        @csrf 
                                        {{ method_field('DELETE') }}
                                        <button type="submit" type="button" class="btn btn-sm btn-danger">Delete</button>
                                    </form>
                                    @endcan
                                </td>
                            </tr>
                            @endforeach
                        </tbody>             
                    </table> 
                </div>
                <div class="card-footer"> </div>
            </div>
        </div>
    </div>
</div>

<!-- Start Edit Modal -->

<div class="modal fade" id="singleModal" tabindex="-1" role="dialog" aria-labelledby="singleModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action=" {{route('admin.users.update', $user->id)}}" method="POST">
                @csrf
                <div id="update-field"></div>

                <div class="modal-header">
                    <h5 class="modal-title" id="singleModalTitle"> </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    @csrf
                    {{ method_field('PUT') }}
                    <input id="userId" type="hidden" name="id" value="">
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right"> Name </label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $user->name }}" required autocomplete="name" autofocus>
                            <span class="invalid-feedback" role="alert">
                                <strong></strong>
                            </span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">Email</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="" required autocomplete="email">
                            <span class="invalid-feedback" role="alert">
                                <strong></strong>
                            </span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right"> User Role </label>
                        <div class="col-md-6">
                            @foreach($roles as $role)
                            <div class="form-check">
                                <input name='roles[]' value="{{$role->id}}" type="checkbox" class="form-check-input" id="checkBox{{$role->id}}">
                                <label class="form-check-label" for="checkBox{{$role->id}}">{{ucfirst($role->name)}}</label>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="saveUser" type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- End Edit Modal -->

@endsection
