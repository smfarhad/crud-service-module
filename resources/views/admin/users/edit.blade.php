@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit User {{$user->name}} </div>
                <div class="card-body">
                 
                <form action="{{route('admin.users.update', $user->id)}}" method="POST">
                    @csrf
                    {{ method_field('PUT') }}
                    <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $user->name }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $user->email }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right"> User Role </label>
                            <div class="col-md-6">
                            @foreach($roles as $role)

                                <div class="form-check">
                                  <input name='roles[]' value="{{$role->id}}" type="checkbox" class="form-check-input" id="checkBox{{$role->id}}"
                                  @if($user->roles->pluck('id')->contains($role->id)) checked @endif >
                                  <label class="form-check-label" for="checkBox{{$role->id}}">{{ucfirst($role->name)}}</label>
                                </div>
                            @endforeach
                            </div>
                        </div>
                    <div class="form-group">
                        <label for="email" class="col-md-4 col-form-label text-md-right"> 
                        <button type="submit" class="btn btn-sm btn-warning"> Update </button>
                        </label>
                        
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
