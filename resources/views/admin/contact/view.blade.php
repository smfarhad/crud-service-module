@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
           
            
            <div class="card">
                <div class="card-header">
                     
                <div class="row">
                    <div class="col-md-4">Contact</div>
                    <div  class="col-md-8 text-right">
                        <a class="btn btn-primary" href="{{route('admin.contacts.index')}}"> View All Contact </a>
                        <a class="btn btn-warning" href="{{route('admin.contacts.edit', $contact->id)}}"> Edit This Contact </a>
                    </div>
                </div>
                
                </div>
                <div class="card-body">


                    @csrf
                    {{ method_field('PUT') }}
                    <input type="hidden" name="id" value="{{ $contact->id }}">
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control"  name="name" value="{{ $contact->name }}" readonly >

                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">Email </label>
                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" value="{{ $contact->email }}" readonly>


                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="phone" class="col-md-4 col-form-label text-md-right">Mobile </label>
                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" value="{{ $contact->phone }}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="city" class="col-md-4 col-form-label text-md-right">City </label>
                        <div class="col-md-6">
                            <input id="city" type="text" class="form-control" name="city" value="{{ $contact->city }}" readonly>


                        </div>
                    </div>                       
                    <div class="form-group row">
                        <label for="city" class="col-md-4 col-form-label text-md-right">Country </label>
                        <div class="col-md-6">
                            <input id="country" type="text" class="form-control" name="country" value="{{ $contact->country }}" readonly>
                        </div>
                    </div>                  
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
