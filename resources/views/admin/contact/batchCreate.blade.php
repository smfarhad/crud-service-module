            @extends('layouts.app')
            @section('content')
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                 
                   
                        <div class="alert alert-danger" role="alert" style="display:none">
                            <div class="row">
                                <div class="col-11">
                                 <ul id="batch-name"></ul>
                            </div>
                            <div class="col-1">
                                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            </div>
                        </div>
                    

                        <div class="card">
                            <div class="card-header">Edit Contact </div>
                            <div class="card-body">
                                <form  id="batch-form" action="{{route('admin.batchcreate')}}" method="POST">
                                    @csrf
                                    <div class="row batch-name-container">
                                        <div class="col-md-2">name</div>
                                        <div class="col-md-3">email</div>
                                        <div class="col-md-2">mobile</div>
                                        <div class="col-md-2">city</div>
                                        <div class="col-md-2">Country</div>
                                        <div class="col-md-1">
                                            <button type="button" class="addNewRow btn btn-sm btn-light"> + </button>
                                        </div>
                                    </div>
                             @for ($i = 0; $i < $numOfRows; $i++)
                                    <div class='row batch-input-container'>
                                        <div class="col-md-2 ">
                                            <input value="{{old('name')}}" type="text" class="form-control form-control-sm @error('name') is-invalid @enderror"  name="contact[{{$i}}][name]" required>
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div> 
                                        <div class="col-md-3">
                                            <input type="email" class="form-control form-control-sm @error('email') is-invalid @enderror" name="contact[{{$i}}][email]"  required >
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-2">
                                            <input type="text" class="form-control form-control-sm @error('phone') is-invalid @enderror" name="contact[{{$i}}][phone]" required>

                                            @error('phone')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class=" col-md-2">
                                            
                                            <input  type="text" class="form-control form-control-sm  @error('phone') is-invalid @enderror" name="contact[{{$i}}][city]" required>

                                            @error('city')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror

                                        </div>
                                        <div class="col-md-2">                                        
                                            <input id="country" type="text" class="form-control-sm form-control @error('country') is-invalid @enderror" name="contact[{{$i}}][country]"  required>
                                            @error('city')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-1">
                                            <button class ="removeRow btn btn-sm btn-danger"> X </button>  
                                        </div>
                                    </div>
                                      
                                    @endfor
                                    <div id="dynamic-container"></div>
                                    <div class=" modal-footer text-center">
                                        <button id="batch-save" type="submit" class="btn btn-primary">Save changes</button>
                                    </div>
                                </form>
                            </div>
                            <div class="card-footer"> </div>
                        </div>
                    </div>
                </div>
            </div>
            @endsection