@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif

            @if (session('error'))
            <div class="alert alert-danger" role="alert">
                {{ session('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif


            <div class="card">
                <div class="card-header">Contact List</div>
                <div class="card-body">
                    <div class="button-container text-right">

                        <!-- Button trigger modal -->
                        <!-- Button trigger modal -->
                        @canany(['manage-contacts','manage-batch-contacts'])
                        
                        <a href="{{route('admin.contacts.create')}}" data-action="{{route('admin.contacts.store')}}"  class="btn btn-sm btn-primary" id="singleCreate">
                            + Add New Contact 
                        </a>
                        @endcan
                        @can('manage-batch-contacts')
                        <a href="{{route('admin.batchcreate')}}" class="btn btn-sm btn-primary batch-create">
                            ++ Add New Contact Batch 
                        </a>
                        <a href="#" class="btn btn-sm btn-warning" id="batchUpdate">
                        Batch Update Contact  
                        </a>
                        <a href="#" class="btn btn-sm btn-danger" id="batchDelete">
                        Batch  Delete Contact
                        </a>
                        @endcan
                    </div>
                    <div class="text-right checkbox-button-container">
                        @canany(['manage-batch-contacts'])
                            <div class="custom-control custom-checkbox">
                                <input value="1" type="checkbox" class="custom-control-input" id="checkAllCheckBox">
                                <label class="custom-control-label" for="checkAllCheckBox">Check All</label>
                            </div>
                        @endcan
                    </div>
                    <div class="data-container">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scopr="col">#</th>
                                    <th scopr="col">Name</th>
                                    <th scopr="col">Email</th>
                                    <th scopr="col">Phone</th>
                                    <th scopr="col">City</th>
                                    <th scopr="col">Country</th>
                                    <th scopr="col">
                                        @canany(['manage-contacts', 'manage-batch-contacts'])
                                            Actions
                                        @endcan
                                    </th>

                                </tr>
                            </thead>
                            <tbody> 

                            @foreach($contacts as $contact)
                              
                                    <tr>
                                        <td socket="row"> {{$contact->id}} </td>
                                        <td> {{$contact->name}} </td>
                                        <td> {{$contact->email}} </td>
                                        <td> {{$contact->phone}} </td>
                                        <td> {{$contact->city}} </td>
                                        <td> {{$contact->country}} </td>
                                        <td class="text-right"> 
                                            @can('manage-batch-contacts')
                                                <div class="form-check float-left">              
                                                    <input name="edit[]" class="form-check-input batch-checkbox" type="checkbox" value="{{$contact->id}}" id="checkdelete{{$contact->id}}">
                                                    <label class="form-check-label" for="checkdelete{{$contact->id}}">
                                                    </label>
                                                </div>   
                                            @endcan     
                                                <a href="{{route('admin.contacts.show', $contact->id)}}" class="float-left btn btn-sm btn-primary">
                                                    view
                                                </a>                   
                                            @can('manage-contacts')
                                                <a data-contact="{{$contact}}" data-action="{{route('admin.contacts.update', $contact->id)}}" href="{{route('admin.contacts.edit', $contact->id)}}" class="btn btn-sm btn-warning edit-button float-left">
                                                    Edit
                                                </a>
                                            @endcan
                                            @can('manage-contacts')
                                                <form action="{{route('admin.contacts.destroy', $contact->id)}}" method="post"  class="float-left delete-button">
                                                    @csrf 
                                                    {{ method_field('DELETE') }}
                                                    <button type="submit" type="button" class="btn btn-sm btn-danger">Delete</button>
                                                </form>
                                            @endcan
                                        </td>
                                    </tr>
                         
                            @endforeach
                            </tbody>
                        </table>
                        <div class="">
                            
                        </div>
                    </div>
                </div>
                <div class="card-footer clearfix">
                        <div class="float-right">
                         {{ $contacts->links() }}
                        
                        </div>
                    
                 </div>
            </div>
        </div>
    </div>
</div>

<!-- Start Edit Modal -->

<div class="modal fade" id="singleModal" tabindex="-1" role="dialog" aria-labelledby="singleModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="{{route('admin.contacts.store')}}" method="POST">
                @csrf
                <div id="update-field"></div>
                
                <div class="modal-header">
                    <h5 class="modal-title" id="singleModalTitle"> </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group row">
                        <label for="name" class="col-md-3 col-form-label text-md-right">{{ __('Name') }}</label>
                        <div class="col-md-8">
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                            <span class="invalid-feedback" role="alert">
                                <strong></strong>
                            </span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-md-3 col-form-label text-md-right">Email </label>
                        <div class="col-md-8">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                            <span class="invalid-feedback" role="alert">
                                <strong></strong>
                            </span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="phone" class="col-md-3 col-form-label text-md-right">Mobile </label>
                        <div class="col-md-8">
                            <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone">                         
                            <span class="invalid-feedback" role="alert">
                                <strong></strong>
                            </span>                         
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="city" class="col-md-3 col-form-label text-md-right">City </label>
                        <div class="col-md-8">
                            <input id="city" type="text" class="form-control @error('city') is-invalid @enderror" name="city" value="{{ old('city') }}" required autocomplete="city">
                            <span class="invalid-feedback" role="alert">
                                <strong></strong>
                            </span>
                        </div>
                    </div>                       
                    <div class="form-group row">
                        <label for="city" class="col-md-3 col-form-label text-md-right">Country </label>
                        <div class="col-md-8">
                            <input id="country" type="text" class="form-control @error('country') is-invalid @enderror" name="country" value="{{ old('contry') }}" required autocomplete="country">

                            <span class="invalid-feedback" role="alert">
                                <strong></strong>
                            </span>
               
                        </div>
                    </div>                  


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button  id="saveContact"  type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- End Edit Modal -->

@endsection
