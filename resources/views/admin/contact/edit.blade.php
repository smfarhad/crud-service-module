@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                
                    <div class="row">
                        <div class="col-md-4">Edit Contact {{$contact->name}} </div>
                        <div  class="col-md-8 text-right">
                            <a class="btn btn-primary" href="{{route('admin.contacts.index')}}"> View All Contact </a>
                            <a class="btn btn-primary" href="{{route('admin.contacts.show', $contact->id)}}"> View this Contact </a>
                        </div>
                    </div>
                
                
                </div>
                <div class="card-body">

                    <form action="{{route('admin.contacts.update', $contact->id)}}" method="POST">
                        @csrf
                        {{ method_field('PUT') }}
                        <input type="hidden" name="id" value="{{ $contact->id }}">
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $contact->name }}" required autocomplete="name" autofocus>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Email </label>
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $contact->email }}" required autocomplete="email">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">Mobile </label>
                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ $contact->phone }}" required autocomplete="phone">

                                @error('phone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="city" class="col-md-4 col-form-label text-md-right">City </label>
                            <div class="col-md-6">
                                <input id="city" type="text" class="form-control @error('phone') is-invalid @enderror" name="city" value="{{ $contact->city }}" required autocomplete="city">

                                @error('city')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>                       
                        <div class="form-group row">
                            <label for="city" class="col-md-4 col-form-label text-md-right">Country </label>
                            <div class="col-md-6">
                                <input id="country" type="text" class="form-control @error('country') is-invalid @enderror" name="country" value="{{ $contact->country }}" required autocomplete="country">

                                @error('city')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>                  
                        <div class="form-group">
                            <label for="email" class="col-md-4 col-form-label text-md-right"> 
                                <button type="submit" class="btn btn-sm btn-warning"> Update </button>
                            </label>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
