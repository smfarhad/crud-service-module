            @extends('layouts.app')

            @section('content')
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                         <div class="alert alert-danger" role="alert" style="display:none">
                            <div class="row">
                                <div class="col-11">
                                 <ul id="batch-name"></ul>
                            </div>
                            <div class="col-1">
                                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">Edit Contact </div>
                            <div class="card-body">
                                <form  id="batch-form" action="{{route('admin.batchupdate')}}" method="POST">
                                    @csrf
                                    <div class="row batch-name-container">
                                    
                                        <div class="col-md-3">name</div>
                                        <div class="col-md-2">email</div>
                                        <div class="col-md-2">mobile</div>
                                        <div class="col-md-2">city</div>
                                        <div class="col-md-2">Country</div>
                                        <div class="col-md-1">
                                            <button type="button" class="addNewRow btn btn-sm btn-light"> + </button>
                                        </div>
                                    </div> 
                                    @php($i = 0)
                                    @foreach($contacts as $contact)
                                    <div class='row batch-input-container'>
                                        <input value="{{$contact->id}}" type="hidden"  name="contact[{{$i}}][id]" >
                                        <div class="col-md-3">
                                            <input value="{{$contact->name}}" type="text" class="form-control form-control-sm @error('name') is-invalid @enderror" name="contact[{{$i}}][name]" required>
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div> 
                                        <div class="col-md-2">
                                            <input value="{{$contact->email}}" type="email" class="form-control form-control-sm @error('email') is-invalid @enderror" name="contact[{{$i}}][email]"  required >
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-2">
                                            <input value="{{$contact->phone}}"  type="text" class="form-control form-control-sm @error('phone') is-invalid @enderror" name="contact[{{$i}}][phone]" required>
                                            @error('phone')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class=" col-md-2">
                                            <input value="{{$contact->city}}"  type="text" class="form-control form-control-sm  @error('phone') is-invalid @enderror" name="contact[{{$i}}][city]" required>
                                            @error('city')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror

                                        </div>
                                        <div class="col-md-2">                                        
                                            <input value="{{$contact->country}}" id="country" type="text" class="form-control-sm form-control @error('country') is-invalid @enderror" name="contact[{{$i}}][country]"  required>
                                            @error('city')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-1">
                                            <button class ="removeRow btn btn-sm btn-danger"> X </button>  
                                        </div>
                                    </div>
                                      @php($i++)
                                    @endforeach
                                    <div id="dynamic-container"></div>
                                    <div class="modal-footer text-center">
                                        <button id="batch-save" type="submit" class="btn btn-primary">Save changes</button>
                                    </div>
                                </form>
                            </div>
                            <div class="card-footer"> </div>
                        </div>
                    </div>
                </div>
            </div>
            @endsection